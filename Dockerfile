FROM ubuntu:14.04

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y unzip wget build-essential \
		cmake git pkg-config libswscale-dev \
		python-numpy \
		libtbb2 libtbb-dev libjpeg-dev \
		libpng-dev libtiff-dev libjasper-dev\
		libopencv-dev python-opencv

